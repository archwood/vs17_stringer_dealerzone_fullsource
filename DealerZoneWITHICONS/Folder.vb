﻿Public Class Folder
    Private _folderpath As String = ""  'path needs to be declared with  on end
    Private _foldername As String
    Private _iconpath As String = "~/Icons/folder.ico"    'requires ico file to be within project

    'CONSTRUCTOR method, only requires path
    Sub New(ByVal path As String)
        _folderpath = path
    End Sub

    Public Property FolderPath() As String
        Get
            Return _folderpath
        End Get
        Set(ByVal value As String)
            _folderpath = value
        End Set
    End Property

    Public Property FolderName() As String
        Get
            _foldername = ""
            Dim pathChar() As Char = _folderpath.ToCharArray
            For i = pathChar.Length - 1 To 0 Step -1
                If pathChar(i) <> "\"c Then
                    _foldername += pathChar(i)
                Else
                    Exit For
                End If
            Next
            _foldername = StrReverse(_foldername)
            Return _foldername
        End Get
        Set(ByVal value As String)
            _foldername = value
        End Set
    End Property

    Public Property IconPath() As String
        Get
            Return _iconpath
        End Get
        Set(ByVal value As String)
            _iconpath = value
        End Set
    End Property
End Class
