
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" Inherits="DealerZoneWITHICONS.login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">Login</title>
    <style type="text/css">
        .auto-style2 {
            z-index: 1;
            left: 396px;
            top: 219px;
            position: absolute;
        }
        .auto-style4 {
            z-index: 1;
            left: 188px;
            top: 123px;
            position: absolute;
        }
        .auto-style5 {
            z-index: 1;
            left: 300px;
            top: 219px;
            position: absolute;
        }
        .auto-style6 {
            z-index: 1;
            left: 440px;
            top: 267px;
            position: absolute;
            width: 115px;
        }
        .auto-style9 {
            z-index: 1;
            left: 441px;
            top: 322px;
            position: absolute;
        }
        .auto-style10 {
            z-index: 1;
            left: 398px;
            top: 144px;
            position: absolute;
        }

        #hyp_reset{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        .auto-style3 {
            z-index: 1;
            left: 10px;
            top: 12px;
            position: absolute;
        }
        .auto-style4 {
            z-index: 1;
            top: 16px;
            position: absolute;
            left: 271px;
            height: 68px;
            float:left;
        }
        #header{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000154;
        }
        #contact{
            float: right;
            margin-top: 25px;
            margin-right: 5px;
            height: 40px;
        }
        #contact img{
            vertical-align:middle;
        }
        .auto-style11 {
            z-index: 1;
            left: 300px;
            top: 180px;
            position: absolute;
            right: 491px;
        }

        .auto-style12 {
            z-index: 1;
            left: 395px;
            top: 182px;
            position: absolute;
            width: 198px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">

            <div id="header" >
                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="auto-style3" Height="79px" ImageUrl="~/logo.jpg" Width="249px" Enabled="False" TabIndex="20" />
                <asp:Label ID="Label3" runat="server" CssClass="auto-style4" Font-Bold="True" Font-Names="Calibri" Font-Size="45pt" ForeColor="#003366" Text="Dealer Zone"></asp:Label>
                <div id="contact">
                    <img src="phone.jpg" width="25" height="25" /> 01438 832052 
                    <img src="email.jpg" width="25" height="25"  />
                    <a href="mailto:sales@stringerscales.co.uk">sales@stringerscales.co.uk</a>
                </div>
            </div>
                <br /><br /><br /><br />
            <div style="float:right;">
                <asp:LinkButton id="hyp_reset" runat="server" TabIndex="5" >Reset Password</asp:LinkButton>
            </div>


        <asp:Label ID="Label2" runat="server" Text="Password" CssClass="auto-style5" Font-Bold="True" Font-Names="Arial"></asp:Label>
        <asp:TextBox ID="txt_password" runat="server" CssClass="auto-style2" TextMode="Password" Width="200px" TabIndex="2" ></asp:TextBox>
        <asp:Button ID="btn_login" runat="server" Text="LOGIN" CssClass="auto-style6" Font-Bold="True" />
        <asp:LinkButton ID="lnk_continue" runat="server" CssClass="auto-style9" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" TabIndex="4">CONTINUE AS GUEST</asp:LinkButton>
        <asp:Label ID="lbl_information" runat="server" CssClass="auto-style10" Font-Bold="True" ForeColor="Red"></asp:Label>
        <asp:Label ID="Label4" runat="server" CssClass="auto-style11" Font-Bold="True" Font-Names="Arial" Text="Email"></asp:Label>
        <asp:TextBox ID="txt_email" runat="server" CssClass="auto-style12" TabIndex ="1"></asp:TextBox>
    </form>
</body>
</html>
