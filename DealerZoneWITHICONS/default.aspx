﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="DealerZoneWITHICONS._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dealer Zone</title>
    <style type="text/css">
        .auto-style3 {
            z-index: 1;
            left: 10px;
            top: 12px;
            position: absolute;
        }
        .auto-style4 {
            z-index: 1;
            top: 16px;
            position: absolute;
            left: 271px;
            height: 68px;
            float:left;
        }
        #header{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000154;
        }
        #contact{
            float: right;
            margin-top: 25px;
            margin-right: 5px;
            height: 40px;
        }
        #contact img{
            vertical-align:middle;
        }
        #copyright{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        #btn_back{
            width:20px;
            height:20px;
            vertical-align:middle;
        }
        #breadcrumb{
            vertical-align:middle;
        }
        #hyp_reset{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        </style>
</head>
<body style="height:100%">

    <form id="mainForm" runat="server" >
        <div>
            <div id="header" >
                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="auto-style3" Height="79px" ImageUrl="~/logo.jpg" Width="249px" />
                <asp:Label ID="Label1" runat="server" CssClass="auto-style4" Font-Bold="True" Font-Names="Calibri" Font-Size="45pt" ForeColor="#003366" Text="Dealer Zone"></asp:Label>
                <div id="contact">
                    <img src="phone.jpg" width="25" height="25" /> 01438 832052 
                    <img src="email.jpg" width="25" height="25"  />
                    <a href="mailto:sales@stringerscales.co.uk">sales@stringerscales.co.uk</a>
                </div>
            </div>
                <br /><br /><br /><br />
            <br /><hr />
            <div>
                <div runat="server" id="breadcrumb" style="display:inline-block;">
                    <%--This is where the breadcrumb trail is generated --%>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div style="display:inline-block;">
                    <%-- Back button --%>
                    <asp:ImageButton ID="btn_back" runat="server" ImageUrl="~/Icons/backup.png" OnClick="btn_back_Click"/>
                </div>
            </div>
            <hr />

            <div id="grids">   <%--This div encapsulates both brid views --%>

                <%-- This is the div for the folder grid view --%>
                <div style="width:300px; height:450px; overflow-y:scroll;border-style:solid;border-width:1px;border-color:black;display:inline-block;">
                   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="folders" runat="server" AutoGenerateColumns = "False"  GridLines="None" OnSelectedIndexChanged="folders_SelectedIndexChanged" OnRowDataBound="folders_RowDataBound" ShowHeader="False" >
                        <Columns>
                            <asp:ImageField DataImageUrlField ="IconPath" >
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:ImageField>
                            <asp:TemplateField>
                              <EditItemTemplate>
                                  <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FolderName") %>'></asp:TextBox>
                              </EditItemTemplate>
                              <ItemTemplate>
                                  <asp:Label ID="Label1" runat="server" Text='<%# Bind("FolderName") %>'></asp:Label>
                              </ItemTemplate>
                              <ControlStyle Width="150px" />
                             </asp:TemplateField>
                            <asp:CommandField selecttext="" ShowSelectButton="true" ButtonType="Link" />
                        </Columns>
                       </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
               </div>

            <%-- This is the div for the file grid view --%>
               <div style="width:800px; height:450px; overflow-y:scroll;border-style:solid;border-width:1px;border-color:black;display:inline-block;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="files" runat="server" AutoGenerateColumns = "False"  GridLines="None" OnSelectedIndexChanged="files_SelectedIndexChanged" OnRowDataBound="files_RowDataBound" CssClass="auto-style5" Width="780px" ShowHeader="False">
                                <Columns>

                                    <asp:ImageField DataImageUrlField ="FileIcon" >
                                         <ControlStyle Height="16px" Width="16px" />
                                    </asp:ImageField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                         <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
                                         </ItemTemplate>
                                            <ControlStyle Width="400px" Height="100%" />
                                     </asp:TemplateField>
                                    <asp:BoundField DataField="FileSize" />
                                    <asp:BoundField DataField="FileDate" />
                                    <asp:CommandField selecttext="" ShowSelectButton="true" ButtonType="Link" >
                                        <ControlStyle Width="2px" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="copyright">
            <p style="text-align:center;"> © Stringer &amp; Co (Scales) Ltd 2017-2022 </p>
        </div>
    </form>
</body>
</html>
