Public Data Web Interface for DealerZone website
Used to display folders and files on public data and allow clients to download file, whilst also having security around files that aren't allowed to be acccessed.


To set passwords for users enter remove 'default.aspx...' from the URL and add 'pwdgen.aspx'
This will take you to another page that you are able to set passwords from by selecting a contact from the list and then choosing whether or not to set the password manually, if not an automatically generated password is made.