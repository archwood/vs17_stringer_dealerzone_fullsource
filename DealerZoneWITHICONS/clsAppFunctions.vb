﻿Public Class clsAppFunctions

#Region "Enumerations, constants, etc."

    Public Enum ErrMsgState
        Critical = 16
        Question = 32
        Exclamation = 48
        Information = 64
    End Enum

#End Region



#Region "Error handling"

    ''' <summary>

    ''' Show message for unhandled exceptions

    ''' </summary>

    ''' <param name="objSender"></param>

    ''' <param name="strMethod"></param>

    ''' <param name="exSource"></param>

    ''' <param name="enErrMsgState"></param>

    ''' <remarks></remarks>

    Public Shared Sub ErrMsg(ByRef objSender As Object, ByVal strMethod As String, ByVal exSource As Exception, Optional enErrMsgState As ErrMsgState = ErrMsgState.Critical)

        Try
            Dim strSenderName As String
            Dim mbs As MsgBoxStyle
            Dim strMessage As String
            If objSender Is Nothing Then
                strSenderName = "clsAppInfo"
            Else
                strSenderName = objSender.Name
            End If
            mbs = enErrMsgState + MsgBoxStyle.OkOnly
            Dim st As New StackTrace(True)
            st = New StackTrace(exSource, True)

            strMessage = "RUN TIME ERROR" & vbCrLf & vbCrLf & "Object: " & strSenderName & vbCrLf &
                "Method: " & strMethod & vbCrLf & vbCrLf &
                "Library: " & exSource.Source.ToString & vbCrLf &
                "Function: " & exSource.TargetSite.ToString & vbCrLf & vbCrLf &
                "Error: " & exSource.Message & vbCrLf & vbCrLf &
                "Line: " & st.GetFrame(0).GetFileLineNumber().ToString
            '& "Error: " & exSource.Message & vbCrLf & vbCrLf _
            '& "Details: " & exSource.InnerException.ToString
            If exSource.InnerException IsNot Nothing Then
                strMessage = strMessage & vbCrLf & vbCrLf & "Details: " & exSource.InnerException.ToString
            End If
            MsgBox(strMessage, mbs)
        Catch ex As Exception
            MsgBox("ErrMsg() error: " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub



#End Region



End Class
