﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pswrdgen.aspx.vb" Inherits="DealerZoneWITHICONS.pswrdgen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Password Generation</title>
    <style type="text/css">
        .auto-style1 {
            z-index: 1;
            left: 15px;
            top: 94px;
            position: absolute;
            width: 414px;
        }
        .auto-style2 {
            z-index: 1;
            left: 18px;
            top: 73px;
            position: absolute;
        }
        .auto-style3 {
            z-index: 1;
            left: 445px;
            top: 273px;
            position: absolute;
            width:206px
        }
        .auto-style4 {
            z-index: 1;
            left: 445px;
            top: 228px;
            position: absolute;
            width: 200px;
        }
        .auto-style5 {
            z-index: 1;
            left: 445px;
            top: 142px;
            position: absolute;
            width: 191px;
        }
        .auto-style6 {
            z-index: 1;
            left: 441px;
            top: 179px;
            position: absolute;
            width: 192px;
        }
        .auto-style7 {
            z-index: 1;
            left: 448px;
            top: 100px;
            position: absolute;
        }
        .auto-style8 {
            z-index: 1;
            left: 17px;
            top: 346px;
            position: absolute;
            height: 200px;
            width: 403px;
            margin-left: 0px;
        }
        .auto-style9 {
            z-index: 1;
            left: 22px;
            top: 321px;
            position: absolute;
            width: 286px;
            right: 510px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btn_emailpwd" runat="server" CssClass="auto-style3" Text="Email password to user"/>
            <asp:Button ID="btn_setpwd" runat="server" CssClass="auto-style4" Text="Set Password" width="206px" />
            <asp:CheckBox ID="chk_manual" runat="server" AutoPostBack="True" CssClass="auto-style5" Text="Manually Set Password" width="206px" />
            <asp:TextBox ID="txt_password" runat="server" CssClass="auto-style6" Visible="False" width="206px"></asp:TextBox>
            <asp:Label ID="lbl_message" runat="server" CssClass="auto-style7" Font-Bold="True" Font-Names="Arial" ForeColor="Red"></asp:Label>
        </div>
        <asp:ListBox ID="lst_contacts" runat="server" AutoPostBack="True" CssClass="auto-style1" Height="204px"></asp:ListBox>
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Larger" Font-Underline="True" Text="Password Generation"></asp:Label>
        <asp:Label ID="Label2" runat="server" CssClass="auto-style2" Text="Choose name from database:"></asp:Label>
        <asp:Label ID="Label3" runat="server" CssClass="auto-style9" Text="Customers that have passwords set:"></asp:Label>
        <asp:ListBox ID="lst_setpasswords" runat="server" CssClass="auto-style8"></asp:ListBox>
    </form>
</body>
</html>
