﻿Imports System.Data.SqlClient

Public Class pswrdgen
    Inherits System.Web.UI.Page
    Public myConn As SqlConnection = New SqlConnection("Data Source=DILBERT\SQL2012DEV;Initial Catalog=ContactsOnSQL;User ID =sa;Password=23640%i")
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader

    Dim password As Integer
    Dim passwordString As String
    Dim _default As New _default
    Dim objEmail As New clsSMTP

    Dim contactData As DataTable = getTable()
    Dim contactDataView As New DataView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lbl_message.Text = ""
        myCmd = myConn.CreateCommand
        Dim cmd As String = "Select ContactName, CompanyID, Email, CompanyName FROM uvw_CompanyContacts WHERE ContactName Is NOT NULL AND CategoryDesc <> 'X'"
        'Code for executing cmd
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
        Catch ex As Exception
            'Displays error message client-side
            clsAppFunctions.ErrMsg(sender, "page_Load()", ex)
            Response.Write("<script type='text/javascript'>alert('An error has occured most likely due to a network connection/the database server being down, apologises for the inconvience.');</script>")
        End Try
        'getting data from query
        Try
            If myReader.HasRows() Then
                Do While myReader.Read()
                    If myReader.GetString(0) <> Nothing Then
                        'adds information for all contact in table to lists so that it is available globally 
                        addContact(myReader, contactData)
                    End If
                Loop
            Else
                MsgBox("Doesn't have rows")
            End If
        Catch ex As Exception
            GoTo end_of_sub 'skips the rest of the function
        End Try

        contactDataView = sortTable(contactData, "ContactName", pswrdgen.SortType.Ascending)    'sorts dataview or should at least
        For Each row In contactDataView
            lst_contacts.Items.Add(row(0).ToString & "  @  " & row(1).ToString)
        Next

        'Closes reader and connection VERY IMPORTANT
end_of_sub:
        Try
            myReader.Close()
            myConn.Close()
        Catch ex As Exception
            'does nothing
        End Try
        getSetCustomers()
    End Sub

    Public Sub getSetCustomers()
        Dim cmd As String = "SELECT ContactName, Email from Contact WHERE EncPassword IS NOT NULL ORDER BY ContactName"
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
            If myReader.HasRows() Then
                Do While myReader.Read()
                    If myReader.GetString(0) <> Nothing And myReader.GetString(1) <> Nothing Then
                        'adds information for all contact in table to lists so that it is available globally 
                        lst_setpasswords.Items.Add(myReader.GetString(0) + " --> " + myReader.GetString(1))
                    End If
                Loop
            Else
                MsgBox("Doesn't have rows")
            End If
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
        myReader.Close()
        myConn.Close()
    End Sub
    'Gives feedback on contact selection with email status
    Protected Sub lst_contacts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lst_contacts.SelectedIndexChanged
        Dim message As String = contactDataView(lst_contacts.SelectedIndex)(3)
        If Not checkEmail(message) Then
            message = "Not found"
        End If
        lbl_message.Text = "Contact selected;  Email: " & message
    End Sub

    'Sets txt_password's visibility depending on whether password is being set automatically or manually
    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chk_manual.CheckedChanged
        If chk_manual.Checked = True Then
            txt_password.Visible = True
        Else
            txt_password.Visible = False
        End If
    End Sub

    'Set the password either automatic of manual and updates SQL record
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_setpwd.Click
        Dim index As Integer = lst_contacts.SelectedIndex
        If txt_password.Visible = True Then
            'Manual password set
            passwordString = txt_password.Text
            password = _default.Encrypt(txt_password.Text)
            txt_password.Text = ""
        Else
            'automatic password generated and set
            Dim name As String = Replace(contactDataView(index)(0).ToString & contactDataView(index)(1).ToString, " ", "")
            passwordString = name.Substring(0, 6).Trim.ToLower & contactDataView(index)(2).ToString
            password = _default.Encrypt(passwordString)
        End If

        'Password cookie
        HttpContext.Current.Response.Cookies("pswrd").Value = passwordString    'sets cookie value to password
        HttpContext.Current.Response.Cookies("pswrd").Expires = DateTime.Now.AddHours(1)


        'connects to database and updates password
        Dim cmd As String = "UPDATE Contact Set EncPassword=" & password & " WHERE ContactName='" & contactDataView(index)(0) & "' AND CompanyID=" & contactDataView(index)(2) & " AND Email='" & contactDataView(index)(3) & "'"
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
            lbl_message.Text = "Password set: " & passwordString    'Feedback
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
        myReader.Close()
        myConn.Close()
    End Sub

#Region "Email"
    'Sends email to client with new password
    Protected Sub btn_emailpwd_Click(sender As Object, e As EventArgs) Handles btn_emailpwd.Click
        Dim index As Integer = lst_contacts.SelectedIndex
        Dim email As String = contactDataView(index)(3).ToString
        If Not checkEmail(email) Then
            lbl_message.Text = "There wasn't a valid email listed for this contact"
            Exit Sub
        End If
        'Creates a SMTP client to send email from using office@stringerscales.co.uk
        objEmail.NewClient(clsSMTP.ServerType.Domain, "STRINGERSERVER", 0, "STRINGER", "office", "P4ssword", False)
        Try
            passwordString = Request.Cookies("pswrd").Value
            objEmail.SendMessage("office@stringerscales.co.uk", email, "", "", "Password Reset", "Your password for the Dealer Zone has been reset to: " & passwordString, True, False)
        Catch ex As Exception
            lbl_message.Text = "Error in sending email, email might be NULL"
        End Try
        lbl_message.Text = "An email has been sent to: " & email
    End Sub

    'Checks to see if email is valid by seeing if it contains an @ symbol
    Public Function checkEmail(target As String) As Boolean
        Dim isValid As Boolean = True
        If Not target.Contains("@") Then
            isValid = False
        End If
        Return isValid
    End Function

#End Region

#Region "Table Procedures"
    'Returns a data table with specified columns
    Public Function getTable() As DataTable
        Dim table As New DataTable
        table.Columns.Add("ContactName", GetType(String))
        table.Columns.Add("CompanyName", GetType(String))
        table.Columns.Add("CompanyId", GetType(String))
        table.Columns.Add("Email", GetType(String))
        Return table
    End Function

    'Adds data into the table, on new row
    Public Sub addContact(reader As SqlDataReader, table As DataTable)
        Dim name, companyName, companyId, email As String
        name = ""
        companyName = ""
        companyId = ""
        If reader.GetString(0) <> Nothing Then
            name = reader.GetString(0)
        End If
        If reader.GetString(3) <> Nothing Then
            companyName = reader.GetString(3)
        End If
        If reader.GetInt32(1) <> Nothing Then
            companyId = reader.GetInt32(1).ToString
        End If
        Try
            email = reader.GetString(2)
        Catch ex As Exception
            email = ""
        End Try
        table.Rows.Add(name, companyName, companyId, email)
    End Sub

    'Sort Types for sortTable()
    Enum SortType
        Ascending = 0
        Descending = 1
    End Enum

    'Returns sorted dataview of specfic type with data from parsed datatable
    Public Function sortTable(table As DataTable, columnName As String, type As pswrdgen.SortType) As DataView
        Dim view = New DataView(table)
        Dim sortString As String = ""
        Select Case type
            Case 0
                sortString = " ASC"
            Case 1
                sortString = " DESC"
        End Select
        view.Sort = columnName & sortString
        Return view
    End Function
#End Region


End Class