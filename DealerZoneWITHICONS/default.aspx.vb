﻿Imports System
Imports System.IO
Imports System.Data.SqlClient 'For SQL connection


Public Class _default
    Inherits System.Web.UI.Page
    'ROOT=The root directory of the folder in whihc you want to browse, needs to be where the web app can access i.e PublicData
    'Const ROOT As String = "\\192.168.0.100\Company\PublicData\" 'EDIT FOR DESIRED ROOT DIRECTORY, needs to have back slash at end
    Dim ROOT As String = My.Settings.Root 'EDIT FOR DESIRED ROOT DIRECTORY, needs to have back slash at end
    Dim directoryString As String = ROOT
    Dim currentDirectories As New List(Of String)
    Dim currentFiles As New List(Of String)
    'SQL OBJECTS
    Private myConn As SqlConnection
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader
    Dim accessFile As AccessFile = New AccessFile() 'For downloading files

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Page).RegisterPostBackControl(files)   'Allows postback through AJAX update panel, download won't work without this
        'Inits home link for breadcrumb
        breadcrumb.InnerHtml() = "<a href='default.aspx'>Dealer Zone</a> / "   'Creates first link on breadcrumb trail to allow you to return home
        'Makes sure both listboxes auto post back, extremely important
        Dim queryString As String = Request.QueryString("currentDir")
        If queryString = "" Then
            'does nothing
        Else
            directoryString = ROOT + queryString + "\"
            Dim breadcrumbTrail() As String = queryString.Split("\"c)
            Dim currentTrail As String = ""
            For i = 0 To breadcrumbTrail.Length - 1
                If breadcrumbTrail(i) = "" Then
                    Continue For
                End If
                currentTrail += breadcrumbTrail(i) & "\"
                breadcrumb.InnerHtml() &= "<a href='default.aspx?currentDir=" & currentTrail & "'>" & breadcrumbTrail(i) & "</a> / "
            Next
        End If
        BindGridFolders(PopulateDataFolders(directoryString))
        'Files binding will go here
        BindGridFiles(PopulateDataFiles(directoryString))
    End Sub

    'LOGIN FUNCTIONALITY
    Public Sub login()
        Dim querystring = Request.QueryString("currentDir")
        Response.Redirect("login.aspx?currentDir=" & querystring)
    End Sub

    Public Function Encrypt(target As String)
        'Encrypt password
        Dim encryptedPassword As Integer
        Dim characters() As Char = target.ToCharArray
        For i = 0 To characters.Length - 1
            encryptedPassword += Asc(characters(i))
        Next
        Return encryptedPassword
    End Function


#Region "Population"
    'POPULATE FUNCTIONS
    Private Function PopulateDataFolders(ByVal path As String) As List(Of Folder)
        Dim folderslist As New List(Of Folder)    'creates new folder list
        For Each dir As String In Directory.GetDirectories(path)
            Dim current As Folder = New Folder(dir) 'creates folder object with path 
            If (current.FolderName.ToCharArray)(0) = "_"c Then
                Continue For 'hides any folders prefixed with '_'
            End If
            folderslist.Add(current) 'folder is added to list
        Next
        Return folderslist
    End Function

    Private Function PopulateDataFiles(ByVal path As String) As List(Of File)
        Dim fileslist As New List(Of File)
        For Each file As String In Directory.GetFiles(path)
            Dim current As File = New File(file)
            If (current.FileName.ToCharArray)(0) = "_"c Then
                Continue For 'hides files with prefix '_'
            End If
            If current.FileName = "locked.txt" Then
                Try
                    If Request.Cookies("l0g1n").Value = "true" Then
                        Continue For 'if already logged in then the file is skipped and access is allowed and locked.txt is not visible
                    End If
                Catch ex As Exception
                    'null cookie doesn't exist yet
                End Try
                login()
                Exit For 'stops loop
            End If
            If current.FileName = "Thumbs.db" Then
                Continue For
            End If
            fileslist.Add(current)
        Next
        Return fileslist
    End Function
#End Region

#Region "Data Bindings"
    'BINDINGS
    Private Sub BindGridFolders(ByVal folderslist As List(Of Folder))
        folders.DataSource = folderslist
        folders.DataBind()
        'For another gridview add duplicate edited code here, use above as template
    End Sub
    Private Sub BindGridFiles(ByVal fileslist As List(Of File))
        files.DataSource = fileslist
        files.DataBind()
    End Sub
#End Region

#Region "Folder/File Onclick handling"
    'FOLDERS ONCLICK
    'Code that is executed when a row is clicked
    Protected Sub folders_SelectedIndexChanged(sender As Object, e As EventArgs) Handles folders.SelectedIndexChanged
        'Gets the object of the selected row
        'sub executed onselect
        Dim list As New List(Of Folder)
        list = folders.DataSource
        Dim current As Folder = list(folders.SelectedIndex)
        directoryString = current.FolderPath
        Dim temp As String = Replace(directoryString, ROOT, "")
        Response.Redirect("default.aspx?currentDir=" + temp)
    End Sub

    'This is code needed for onClick functionality (...I think)
    Protected Sub folders_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles folders.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(folders, "Select$" & e.Row.RowIndex)
            e.Row.Attributes("style") = "cursor:pointer"
        End If
    End Sub

    'FILES ONCLICK
    Public Sub files_SelectedIndexChanged(sender As Object, e As EventArgs) Handles files.SelectedIndexChanged
        'ONCLICK events
        Dim list As New List(Of File)
        list = files.DataSource
        Dim current As File = list(files.SelectedIndex)
        'DOWNLOAD FILE HERE
        accessFile.downloadFileObject(current)
    End Sub

    Protected Sub files_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles files.RowDataBound
        'onclick config
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(files, "Select$" & e.Row.RowIndex)
            e.Row.Attributes("style") = "cursor:pointer"
        End If
    End Sub
#End Region

#Region "Navigation"
    Protected Sub btn_back_Click(sender As Object, e As ImageClickEventArgs) Handles btn_back.Click
        navigate_back()
    End Sub

    'Stringer Scales Logo
    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Response.Redirect("http://www.stringerscales.co.uk/")
    End Sub

    Public Sub navigate_back()
        Dim querystring As String = HttpContext.Current.Request.QueryString("currentDir")
        Dim querychar() As Char = {}
        Try
            querychar = querystring.ToCharArray
        Catch
            'does nothing
        End Try
        Dim backstring As String = ""
        If querystring <> "" Then
            For x = querychar.Length - 1 To 0 Step -1
                If x = 0 Then
                    backstring = querystring
                    Exit For
                End If
                If querychar(x) <> "\"c Then
                    backstring &= querychar(x)
                Else
                    backstring &= querychar(x)
                    Exit For
                End If
            Next
            If backstring = querystring Then
                querystring = Replace(querystring, backstring, "")
            Else
                backstring = StrReverse(backstring)
                querystring = Replace(querystring, backstring, "")
            End If
            HttpContext.Current.Response.Redirect("default.aspx?currentDir=" & querystring)
        End If
    End Sub
#End Region

End Class