﻿Imports Microsoft.VisualBasic
Imports System.IO

Public Class AccessFile
    Inherits System.Web.UI.Page
    'Added Overloads to get rid of warning
    Public Overloads Sub openFile(path As String)
        Dim pathChar() As Char = path.ToCharArray()
        Dim fileExtension As String = ""
        For i = path.Length - 1 To 0 Step -1
            If pathChar(i) = "."c Then
                Exit For
            ElseIf pathChar(i) <> "."c Then
                fileExtension += pathChar(i)
            End If
            fileExtension = StrReverse(fileExtension)
        Next
        Dim contentType As String = ""
        'Selects correct MIME type for file extension
        Select Case fileExtension.ToLower
            Case "pdf"
                contentType = "application/pdf"
            Case "jpg", "jpeg"
                contentType = "image/jpg"
            Case "txt"
                contentType = "text"
            Case "doc", "dot"
                contentType = "application/msword"
        End Select
        HttpContext.Current.Response.ContentType = contentType
        HttpContext.Current.Response.WriteFile(path)
        HttpContext.Current.Response.Flush()
    End Sub

    'Can be reused in the future
    Public Sub downloadFile(path As String)
        Dim pathChar() As Char = path.ToCharArray()
        Dim fileExtension As String = ""
        Dim filename As String = ""

        For i = path.Length - 1 To 0 Step -1
            If pathChar(i) = "."c Then
                Exit For
            ElseIf pathChar(i) <> "."c Then
                fileExtension += pathChar(i)
            End If
        Next
        fileExtension = StrReverse(fileExtension)
        For i = pathChar.Length - 1 To 0 Step -1
            If pathChar(i) = "\"c Then
                Exit For
            ElseIf pathChar(i) <> "\"c Then
                filename += pathChar(i)
            End If
        Next
        filename = Replace(filename, "." & fileExtension, "")
        filename = StrReverse(filename)
        Dim contentType As String = ""
        'For this function could possibly change to Substring as most file extensions are only three characters
        Select Case fileExtension.ToLower
            Case "pdf"
                contentType = "application/pdf"
            Case "jpg", "jpeg"
                contentType = "image/jpg"
            Case "txt"
                contentType = "text"
        End Select
        'Sets file  type, is required otherwise file is damaged when downloading
        HttpContext.Current.Response.ContentType = contentType
        'REMINDER: needs file extension in filename to save as the proper file
        HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + filename)
        'This is where you download the file
        HttpContext.Current.Response.TransmitFile(path)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Close()    ''Testing for bug fix

    End Sub

    Public Sub downloadFileObject(file As File)
        Dim contentType As String = ""
        Select Case file.FileExtension.ToLower
            Case "pdf"
                contentType = "application/pdf"
            Case "jpg", "jpeg"
                contentType = "image/jpg"
            Case "txt"
                contentType = "text"
        End Select
        HttpContext.Current.Response.ContentType = contentType
        'REMINDER: needs file extension in filename to save as the proper file
        HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + file.FileName)
        'This is where you download the file
        HttpContext.Current.Response.TransmitFile(file.FilePath)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Close()
    End Sub

    Public Function getFileSize(path As String, format As String)
        Dim infoReader As System.IO.FileInfo
        infoReader = My.Computer.FileSystem.GetFileInfo(path)
        Dim bytes As Double = infoReader.Length
        Select Case format
            Case "b"
                Return Decimal.Round(Convert.ToDecimal(bytes), 2)
            Case "k"
                Return Decimal.Round(Convert.ToDecimal(bytes / 1024), 2)
            Case "m"
                Return Decimal.Round(Convert.ToDecimal((bytes / 1024) / 1024), 2)
            Case "g"
                Return Decimal.Round(Convert.ToDecimal(((bytes / 1024) / 1024) / 1024), 2)
            Case Else
                Return 0
                'error ocurred
        End Select
    End Function
End Class
