﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="reset.aspx.vb" Inherits="DealerZoneWITHICONS.reset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        .auto-style3 {
            z-index: 1;
            left: 10px;
            top: 12px;
            position: absolute;
        }
        .auto-style4 {
            z-index: 1;
            top: 16px;
            position: absolute;
            left: 271px;
            height: 68px;
            float:left;
        }
        #header{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000154;
        }
        #lbl_information{
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 13px;
        }
        #contact{
            float: right;
            margin-top: 25px;
            margin-right: 5px;
            height: 40px;
        }
        #contact img{
            vertical-align:middle;
        }
        .auto-style5 {
            z-index: 1;
            left: 547px;
            top: 190px;
            position: absolute;
        }
        .auto-style6 {
            z-index: 1;
            left: 281px;
            top: 174px;
            position: absolute;
        }
        .auto-style7 {
            z-index: 1;
            left: 278px;
            top: 137px;
            position: absolute;
        }
        .auto-style8 {
            z-index: 1;
            left: 392px;
            top: 233px;
            position: absolute;
        }
     </style>

    <title>Password Reset</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
            <div id="header" >
                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="auto-style3" Height="79px" ImageUrl="~/logo.jpg" Width="249px" Enabled="False" />
                <asp:Label ID="Label1" runat="server" CssClass="auto-style4" Font-Bold="True" Font-Names="Calibri" Font-Size="45pt" ForeColor="#003366" Text="Dealer Zone"></asp:Label>
                <asp:Label ID="Label2" runat="server" CssClass="auto-style6" Text="Enter email address:"></asp:Label>
                <div id="contact">
                    <img src="phone.jpg" width="25" height="25" /> 01438 832052 
                    <img src="email.jpg" width="25" height="25"  />
                    <a href="mailto:sales@stringerscales.co.uk">sales@stringerscales.co.uk</a>
                </div>
            </div>
        </div>
        <asp:TextBox ID="txt_email" runat="server" style="z-index: 1; left: 279px; top: 191px; position: absolute; width: 254px; margin-top: 1px;" TextMode="Email" TabIndex="1"></asp:TextBox>
        <asp:Button ID="btn_reset" runat="server" Text="Reset" CssClass="auto-style5" TabIndex="2" />
        <asp:Label ID="lbl_information" runat="server" CssClass="auto-style7" Font-Bold="True" ForeColor="Red"></asp:Label>
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="auto-style8" Font-Bold="True">Go back</asp:LinkButton>
    </form>
</body>
</html>
