﻿Public Class File
    Private _filepath As String
    Private _filename As String
    Private _filesize As String
    Private _filedate As String
    Private _fileextension As String
    Private _fileicon As String

    Dim infoReader As IO.FileInfo

    'CONSTRUCTOR method, only requires path
    Sub New(ByVal path As String)
        _filepath = path
        infoReader = My.Computer.FileSystem.GetFileInfo(_filepath)
    End Sub

    Public Property FilePath() As String
        Get
            Return _filepath
        End Get
        Set(value As String)
            _filepath = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            _filename = ""
            Dim pathChar() As Char = _filepath.ToCharArray
            For i = pathChar.Length - 1 To 0 Step -1
                If pathChar(i) <> "\"c Then
                    _filename += pathChar(i)
                Else
                    Exit For
                End If
            Next
            _filename = StrReverse(_filename)
            Return _filename
        End Get
        Set(value As String)
            '_filename = value
        End Set
    End Property

    Public Property FileSize() As String
        Get
            Dim accessfile As AccessFile = New AccessFile
            Dim size As Double = accessfile.getFileSize(_filepath, "m")
            If size < 1 Then
                size = accessfile.getFileSize(_filepath, "k")
                _filesize = size.ToString & " kB"
            Else
                _filesize = size.ToString & " MB"
            End If
            Return _filesize
        End Get
        Set(value As String)
            '_filesize=value
        End Set
    End Property

    Public Property FileDate() As String
        Get
            'Gets date modified
            _filedate = infoReader.LastAccessTime.ToString
            Return _filedate
        End Get
        Set(value As String)
            '_filedate=value
        End Set
    End Property

    Public Property FileExtension() As String
        Get
            _fileextension = ""
            Dim pathChar() As Char = _filepath.ToCharArray()
            For i = pathChar.Length - 1 To 0 Step -1
                If pathChar(i) = "."c Then
                    Exit For
                ElseIf pathChar(i) <> "."c Then
                    _fileextension += pathChar(i)
                End If
            Next
            _fileextension = StrReverse(_fileextension)
            Return _fileextension
        End Get
        Set(value As String)
            '_fileextension=value
        End Set
    End Property

    Public Property FileIcon() As String
        Get
            Dim ext As String = FileExtension().ToLower
            Select Case ext
                Case "pdf"
                    _fileicon = "~/Icons/pdf.ico"
                Case "jpg", "jpeg", "png"
                    _fileicon = "~/Icons/image.ico"
                Case "txt"
                    _fileicon = "~/Icons/textdoc.ico"
                Case "zip"
                    _fileicon = "~/Icons/zipped.ico"
                Case "exe"
                    _fileicon = "~/Icons/exe.ico"
                Case Else
                    _fileicon = "~/Icons/document.ico"
            End Select
            Return _fileicon
        End Get
        Set(value As String)
            _fileicon = value
        End Set
    End Property

End Class
