﻿Imports System.Data.SqlClient

Public Class reset
    Inherits System.Web.UI.Page
    Dim login As New login
    Dim main As New _default
    Public objEmail As New clsSMTP


    Private myConn As SqlConnection = login.myConn   'uses same connection as login
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader


    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_reset.Click
        Dim email As String = txt_email.Text
        If email = "" Then
            lbl_information.Text = "An email address is required"
            Exit Sub
        End If
        Dim name As String = ""
        Dim companyId As String = ""

        'Code to change password using the password get function from pswrdgen.aspx
        myCmd = myConn.CreateCommand
        Dim cmd As String = "Select ContactName, CompanyID FROM Contact WHERE ContactName Is NOT NULL AND Email='" & email & "';"
        'Code for executing cmd
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        'getting data from query
        Try
            If myReader.HasRows() Then
                Do While myReader.Read()
                    If myReader.GetString(0) <> Nothing Then
                        name = (myReader.GetString(0))
                        companyId = (myReader.GetInt32(1).ToString)
                    End If
                Loop
            Else
                MsgBox("Email not found")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            GoTo end_of_sub 'skips the rest of the function
        End Try
        'Closes reader and connection VERY IMPORTANT
end_of_sub:
        myReader.Close()
        myConn.Close()


        Dim newPassword As String = companyId & name.ToLower
        newPassword = Replace(newPassword, " ", "") 'gets rid of whitespace
        'update password where email = email
        cmd = "UPDATE Contact SET EncPassword=" & main.Encrypt(newPassword) & " WHERE Email='" & email & "' AND EncPassword IS NOT NULL"
        Dim cmd1 = "SELECT EncPassword from Contact where Email='" & email & "' AND ContactName='" & name & "'"
        Dim changed As Boolean = False

        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
            'Password added to database
            myReader.Close()
            myConn.Close()

            myCmd.CommandText = cmd1
            myConn.Open()
            myReader = myCmd.ExecuteReader()
            If myReader.HasRows() Then
                Do While myReader.Read()
                    If myReader.GetString(0) <> Nothing Then
                        'adds information for all contact in table to lists so that it is available globally 
                        changed = True
                    End If
                Loop
            Else
                'old msg feedback
            End If

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
        myReader.Close()
        myConn.Close()
        If changed Then
            'code to email password to the email address saved in the database
            objEmail.NewClient(clsSMTP.ServerType.Domain, "STRINGERSERVER", 0, "STRINGER", "office", "P4ssword", False)
            objEmail.SendMessage("office@stringerscales.co.uk", email, "", "", "Password Reset", "Your password for the Dealer Zone has been reset to: " & newPassword, True, False)
            lbl_information.Text = "You have been sent an email with your reset password"
            main.navigate_back()    'sends user back to where they came from
        Else
            lbl_information.Text = "Your logon hasnt been set up, please contact the office"
        End If

    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Response.Redirect("http://www.stringerscales.co.uk/")
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        main.navigate_back()

    End Sub

    Private Sub form1_Load(sender As Object, e As EventArgs) Handles form1.Load
        txt_email.Focus() 'set focus on text box
    End Sub
End Class