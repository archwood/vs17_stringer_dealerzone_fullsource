﻿Imports System.Data.SqlClient 'For SQL connection

Public Class login
    Inherits System.Web.UI.Page
    Public myConn As SqlConnection = New SqlConnection("Data Source=DILBERT\SQL2012DEV;Initial Catalog=ContactsOnSQL;User ID =sa;Password=23640%i")
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader

    Dim main As _default = New _default 'used for encrypt

    'Login button
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_login.Click
        lbl_information.Text = ""
        Dim email As String = txt_email.Text
        Dim encPass As Integer = main.Encrypt(txt_password.Text)  'Encrypts user's input password
        myCmd = myConn.CreateCommand
        Dim cmd As String = "Select EncPassword FROM Contact WHERE Email='" + email + "'"
        'Code for executing cmd
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
        Catch ex As Exception
            Console.WriteLine("Exception in query", MsgBoxStyle.Critical)
        End Try

        Dim databasePassword As String = ""
        'getting data from query
        Try
            If myReader.HasRows() Then
                Do While myReader.Read()
                    databasePassword = myReader.GetString(0)
                Loop
            Else
                Console.WriteLine("Doesn't have rows")
            End If
        Catch ex As Exception
            Console.WriteLine("Password Not Found or Email incorrect")
            lbl_information.Text = "Password Not Found or Email incorrect"
            GoTo end_of_sub 'skips the rest of the function
        End Try
        If encPass = 0 Then
            lbl_information.Text = "A password is required"
            GoTo end_of_sub

        End If
        Try
            If encPass = Int(Convert.ToInt32(databasePassword)) Then
                'Valid login and allow folder access
                'COOKIE MANAGEMENT
                HttpContext.Current.Response.Cookies("l0g1n").Value = "true"    'sets cookie value to true
                HttpContext.Current.Response.Cookies("l0g1n").Expires = DateTime.Now.AddHours(1) 'keeps user logged in for 1 hour then will have to sign back in
                HttpContext.Current.Response.Redirect("default.aspx?currentDir=" & HttpContext.Current.Request.QueryString("currentDir"))
                ' use a cookie to show logged in and check cookie in _default.updateFiles() before checking for locked.txt file
            ElseIf Int(Convert.ToInt32(databasePassword)) = 0 Then
                Console.WriteLine("A Password has not been registered to your account")
                lbl_information.Text = "A Password has not been registered to your account"
            Else
                'Invalid login show message and clear txt_password 
                Console.WriteLine("Invalid Credentials")
                lbl_information.Text = "Invalid Credentials"
                txt_password.Text = ""
            End If
        Catch ex As FormatException
            lbl_information.Text = "Invalid credentials"
            GoTo end_of_sub

        End Try

        'Closes reader and connection
end_of_sub:
        myReader.Close()
        myConn.Close()
    End Sub

#Region "Buttons"
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles lnk_continue.Click
        'sends user back to specific directory viewed
        main.navigate_back()
    End Sub

    Protected Sub hyp_reset_Click(sender As Object, e As EventArgs) Handles hyp_reset.Click
        Dim querystring As String = Request.QueryString("currentDir")
        Response.Redirect("reset.aspx?currentDir=" & querystring)   'sends user to the reset password page
    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Response.Redirect("http://www.stringerscales.co.uk/")   'home button
    End Sub
#End Region

    Protected Sub form1_Load(sender As Object, e As EventArgs) Handles form1.Load
        txt_email.Focus()   'sets focus on email field
    End Sub
End Class